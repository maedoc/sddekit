/* copyright 2016 Apache 2 sddekit authors */

#include "sddekit.h"

#define TEST_FOUND(name) int name();
#include "test_list.h"
#undef TEST_FOUND

int sd_test_run_all() {
#define TEST_FOUND(name) name();
#include "test_list.h"
#undef TEST_FOUND

	sd_log_set_verbose(1);
	return sd_test_report();
}
