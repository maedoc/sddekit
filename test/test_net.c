/* copyright 2016 Apache 2 sddekit authors */

#include <stdlib.h>

#include "sddekit.h"

TEST(net, simple) {

	int n, ns, ne, nnz, *Ic, *Or, *vi;
	double *w, *d, *x, *f, *g, *c, *vd;
	sd_net_data *net;
	sd_sys_exc_dat *sysd;
	sd_hist *hist;

	n = 3;
	ns = 2;
	ne = 1;
	nnz = 2;
	Or = sd_malloc (sizeof(int)*(n+1));
	Ic = sd_malloc (sizeof(int)*nnz);
	w = sd_malloc (sizeof(double)*nnz);
	d = sd_malloc (sizeof(double)*nnz);
	Or[0] = 0;
	Or[1] = 1;
	Or[2] = 2;
	Or[3] = 2;
	Ic[0] = 1;
	Ic[1] = 2;
	w[0] = 1.0;
	w[1] = 2.0;
	d[0] = 10.0;
	d[1] = 20.0;
	sysd = sd_sys_exc_alloc();
	sd_sys_exc_set_a(sysd, 1.0);
	sd_sys_exc_set_k(sysd, 0.5);
	sd_sys_exc_set_tau(sysd, 3.0);
	vi = sd_malloc (sizeof(int)*n);
	vd = sd_malloc (sizeof(double)*n);
	vi[0] = 0;
	vi[1] = 1;
	vi[2] = 2;
	vd[0] = 0.0;
	vd[1] = 0.0;
	vd[2] = 0.0;
	hist = sd_hist_alloc();
	sd_hist_init(hist, n, vi, vd, 0, 1);

	net = sd_net_alloc();
	sd_net_init1(net, n, sd_sys_exc, sysd, ns, ne, nnz, Or, Ic, w, d);

	/* initn */
	EXPECT_EQ(n,sd_net_get_n(net));
	EXPECT_EQ(1,sd_net_get_m(net));
	EXPECT_EQ(nnz,sd_net_get_nnz(net));
	EXPECT_EQ(Or,sd_net_get_or(net));
	EXPECT_EQ(Ic,sd_net_get_ic(net));
	EXPECT_EQ(w,sd_net_get_w(net));
	EXPECT_EQ(d,sd_net_get_d(net));
	EXPECT_EQ(ns*n,sd_net_get_ns(net));
	EXPECT_EQ(ne*n,sd_net_get_ne(net));
	EXPECT_TRUE(!sd_net_cn_is_null(net));

	/* init1 */
	EXPECT_EQ(ns, sd_net_get_Ms_i(net, 0));
	EXPECT_EQ(ne, sd_net_get_Me_i(net, 0));
	EXPECT_EQ(0, sd_net_get_M_i(net, 0));
	EXPECT_EQ(0, sd_net_get_M_i(net, 1));
	EXPECT_EQ(0, sd_net_get_M_i(net, 1));
	EXPECT_EQ(&sd_sys_exc, sd_net_get_models_i(net, 0));
	EXPECT_EQ(sysd, sd_net_get_models_data_i(net, 0));
	EXPECT_EQ(1, sd_net_get__init1(net));

	/* evaluate */
	x = sd_malloc (sizeof(double) * n*ns);
	f = sd_malloc (sizeof(double) * n*ns);
	g = sd_malloc (sizeof(double) * n*ns);
	c = sd_malloc (sizeof(double) * n*ne);
	c[0] = x[0] = 1.0;
	c[1] = x[2] = 2.0;
	c[2] = x[4] = 3.0;
	sd_net_sys(net, hist, 0.0, 0, n*ns, x, f, g, NULL, NULL, n*ne, c, NULL, NULL);
	EXPECT_EQ((sd_sys_exc_get_a(sysd) - x[0] + sd_sys_exc_get_k(sysd)*w[0]*x[2])/sd_sys_exc_get_tau(sysd),f[1]);
	EXPECT_EQ((sd_sys_exc_get_a(sysd) - x[2] + sd_sys_exc_get_k(sysd)*w[1]*x[4])/sd_sys_exc_get_tau(sysd),f[3]);
	EXPECT_EQ((sd_sys_exc_get_a(sysd) - x[4])/sd_sys_exc_get_tau(sysd),f[5]);

	/* clean up */
	sd_sys_exc_free(sysd);
	sd_net_free(net);
	sd_hist_free(hist);
	sd_free(Or);
	sd_free(Ic);
	sd_free(w);
	sd_free(d);
	sd_free(x);
	sd_free(f);
	sd_free(g);
	sd_free(c);
	sd_free(vi);
	sd_free(vd);
}
