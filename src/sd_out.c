/* copyright 2016 Apache 2 sddekit authors */

#include "sddekit.h"

#define RETNULL(p, v) if ((p)==NULL) { \
	sd_err("returning early due to NULL pointer."); \
	return v; \
}

/* write data to file */

struct sd_out_file_data {
	FILE *fd;
	int isstd;
};

sd_out_file_data *sd_out_file_alloc() {
	sd_out_file_data *new, zero = {0};
	new = sd_malloc (sizeof(sd_out_file_data));
	*new = zero;
	return new;
}

int sd_out_file_is_std(sd_out_file_data *d) {
	RETNULL(d, 0);
	return d->isstd;
}

int sd_out_file_from_fname(sd_out_file_data *d, char *fname) {
	RETNULL(d, 1);
	d->fd = fopen(fname, "w");
	if (d->fd==NULL) {
		sd_err("unable to open file for writing.");
		return 1;
	}
	d->isstd = 0;
	return d->fd==NULL;
}

int sd_out_file_from_std(sd_out_file_data *d, FILE *std) {
	RETNULL(d, 1);
	if (!(std==stdout || std==stderr)) {
		sd_err("std file is neither stdout nor stderr");
		return 1;
	}
	d->fd = std;
	d->isstd = 1;
	return 0;
}

SK_DEFOUT(sd_out_file) {
	int i;
	sd_out_file_data *d = data;
	RETNULL(d, 1);
	fprintf(d->fd, "%f %d ", t, nx);
	for (i=0; i<nx; i++) fprintf(d->fd, "%f ", x[i]);
	fprintf(d->fd, "%d ", nc);
	for (i=0; i<nc; i++) fprintf(d->fd, "%f ", c[i]);
	fprintf(d->fd, "\n");
	return 1;
}

void sd_out_file_free(sd_out_file_data *d) {
	if (d==NULL) {
		sd_err("cowardly refusing to free NULL pointer.");
		return;
	}
	if (!(d->isstd))
		fclose(d->fd);
	sd_free(d);
}

/* auto-allocating array */
struct sd_out_mem_data {
	int n_sample, capacity, nx, nc;
	double *xs, *cs;
};

sd_out_mem_data *sd_out_mem_alloc() {
	sd_out_mem_data *new, zero = {0};
	new = sd_malloc (sizeof(sd_out_mem_data));
	*new = zero;
	return new;
}

int sd_out_mem_get_n_sample(sd_out_mem_data *d) {
	return d->n_sample;
}

int sd_out_mem_get_nx(sd_out_mem_data *d) {
	return d->nx;
}

int sd_out_mem_get_nc(sd_out_mem_data *d) {
	RETNULL(d, 0);
	return d->nc;
}

double *sd_out_mem_get_xs(sd_out_mem_data *d) {
	RETNULL(d, NULL);
	return d->xs;
}

double *sd_out_mem_get_cs(sd_out_mem_data *d) {
	RETNULL(d, NULL);
	return d->cs;
}

int sd_out_mem_init(sd_out_mem_data *d) {
	RETNULL(d, 1);
	d->xs = NULL;
	d->cs = NULL;
	d->n_sample = 0;
	d->capacity = 0;
	return 0;
}

SK_DEFOUT(sd_out_mem) {
	sd_out_mem_data *d = data;
	(void) t;
	if (d->capacity==0) {
		int err;
		/* first alloc */
		d->nx = nx;
		d->nc = nc;
		err = (d->xs = sd_malloc (sizeof(double)*nx))==NULL;
	       	err |= (d->cs = sd_malloc (sizeof(double)*nc))==NULL;
		if (err) {
			sd_err("failed to allocate memory.");
			if (d->xs!=NULL) sd_free(d->xs);
			if (d->cs!=NULL) sd_free(d->cs);
			return 0;
		}
		d->capacity = 1;
	}
	memcpy(d->xs + d->n_sample*nx, x, sizeof(double)*nx);
	memcpy(d->cs + d->n_sample*nc, c, sizeof(double)*nc);
	d->n_sample++;
	if (d->n_sample==d->capacity) {
		double *x_, *c_;
		/* expand buffer */
		x_ = sd_realloc (d->xs, sizeof(double)*nx*2*d->capacity);
		if (x_==NULL) {
			sd_err("failed to allocate memory.");
			return 0;
		} else {
			/* save new ptr in case c_ realloc fails, free can still
			 * handle x_ 
			 */
			d->xs = x_;
		}
		c_ = sd_realloc (d->cs, sizeof(double)*nc*2*d->capacity);
		if (c_==NULL) {
			sd_err("failed to allocate memory.");
			return 0;
		}
		d->capacity *= 2;
		d->xs = x_;
		d->cs = c_;
	}
	return 1;
}

void sd_out_mem_free(sd_out_mem_data *d) {
	RETNULL(d, );
	if (d->xs!=NULL)
		sd_free(d->xs);
	if (d->cs!=NULL)
		sd_free(d->cs);
	sd_free(d);
}

/* split n-ways */

struct sd_out_tee_data {
	int nout;
	sd_out *outs;
	void **outd;
};

sd_out_tee_data *sd_out_tee_alloc() {
	sd_out_tee_data *new, zero = {0};
	new = sd_malloc (sizeof(sd_out_tee_data));
	*new = zero;
	return new;
}

int sd_out_tee_init(sd_out_tee_data *d, int nout) {
	int err;
	d->nout = nout;
	err = (d->outs = sd_malloc (sizeof(sd_out) * nout))==NULL;
	err |= (d->outd = sd_malloc (sizeof(void*) * nout))==NULL;
	if (err) {
		sd_err("unable to allocate memory.");
		if (d->outs!=NULL) sd_free(d->outs);
		if (d->outd!=NULL) sd_free(d->outd);
		return 1;
	}
	return 0;
}

int sd_out_tee_set_out(sd_out_tee_data *d, int i, sd_out out, void *data) {
	RETNULL(d, 1);
	if (i < d->nout) {
		d->outs[i] = out;
		d->outd[i] = data;
		return 0;
	}
	sd_err("out of bounds index for splitter output.");
	return 1;
}

SK_DEFOUT(sd_out_tee) {
	int i, flag;
	sd_out_tee_data *d = data;
	flag = 1;
	for (i=0; i<d->nout; i++)
		flag &= (*(d->outs[i]))(d->outd[i], t, nx, x, nc, c);
	return flag;
}

void sd_out_tee_free(sd_out_tee_data *d) {
	RETNULL(d, );
	sd_free(d->outs);
	sd_free(d->outd);
	sd_free(d);
}

/* temporal average / downsample */

struct sd_out_tavg_data {
	int pos, len;
	double *x, *c, t;
	sd_out out;
	void *outd;
};

sd_out_tavg_data *sd_out_tavg_alloc() {
	sd_out_tavg_data *new, zero = {0};
	new = sd_malloc (sizeof(sd_out_tavg_data));
	*new = zero;
	return new;
}

int sd_out_tavg_init(sd_out_tavg_data *d, int len, sd_out out, void *outd) {
	RETNULL(d, 1);
	d->len = len;
	d->pos = 0;
	d->t = 0.0;
	if (out==NULL) {
		sd_err("tavg passed NULL output.");
		return 1;
	}
	d->out = out;
	d->outd = outd;
	d->x = NULL;
	d->c = NULL;
	return 0;
}

SK_DEFOUT(sd_out_tavg) {
	int i;
	int flag;
	sd_out_tavg_data *d;
	d = data;
#define ALL(lim) for(i=0;i<(lim);i++)
	flag = 0; 
	if (d->x==NULL) {
		flag = (d->x = sd_malloc (sizeof(double) * nx))==NULL;
		flag |= (d->c = sd_malloc (sizeof(double) * nc))==NULL;
		if (flag) {
			if (d->x) sd_free(d->x);
			if (d->c) sd_free(d->c);
			sd_err("failed to allocate memory for temporal average buffer.");
			return 0;
		}
		ALL(nx) d->x[i] = 0.0;
		ALL(nc) d->c[i] = 0.0;
	}
	flag = 1;
	ALL(nx) d->x[i] += x[i];
	ALL(nc) d->c[i] += c[i];
	d->t += t;
	d->pos++;
	if (d->pos==d->len) {
		ALL(nx) d->x[i] /= d->len;
		ALL(nc) d->c[i] /= d->len;
		d->t /= d->len;
		flag = (*(d->out))(d->outd, d->t, nx, d->x, nc, d->c);
		d->pos = 0;
		ALL(nx) d->x[i] = 0.0;
		ALL(nc) d->c[i] = 0.0;
		d->t = 0.0;
	}
#undef ALL
	return flag;
}

void sd_out_tavg_free(sd_out_tavg_data *d) {
	RETNULL(d, );
	if (d->x!=NULL) sd_free(d->x);
	if (d->c!=NULL) sd_free(d->c);
	sd_free(d);
}

/* spatial filter (bank) 
 *
 * xfilts laid out as filter per row, i.e. shape [nfilt nx] & [nfilt nc]
 */

struct sd_out_sfilt_data {
	int nfilt, filtlen;
	double *xfilts, *cfilts, *x, *c;
	sd_out out;
	void *outd;
};

sd_out_sfilt_data *sd_out_sfilt_alloc() {
	sd_out_sfilt_data *new, zero = {0};
	new = sd_malloc (sizeof(sd_out_sfilt_data));
	*new = zero;
	return new;
}

int sd_out_sfilt_init(sd_out_sfilt_data *d, int nfilt, int filtlen, 
		double *xfilts, double *cfilts, sd_out out, void *outd) {
	int nb, err;
	d->nfilt = nfilt;
	d->filtlen = filtlen;
	nb = nfilt * filtlen * sizeof(double);
	err = (d->xfilts = sd_malloc(nb))==NULL;
	err |= (d->cfilts = sd_malloc(nb))==NULL;
	err |= (d->x = sd_malloc (sizeof(double) * nfilt))==NULL;
	err |= (d->c = sd_malloc (sizeof(double) * nfilt))==NULL;
	if (err) {
		if (d->xfilts!=NULL) sd_free(d->xfilts);
		if (d->cfilts!=NULL) sd_free(d->cfilts);
		if (d->x!=NULL) sd_free(d->x);
		if (d->c!=NULL) sd_free(d->c);
		sd_err("failed to allocate memory during spatial filter allocation.");
		return 1;
	}
	memcpy(d->xfilts, xfilts, nb);
	memcpy(d->cfilts, cfilts, nb);
	d->out = out;
	d->outd = outd;
	return 0;
}

SK_DEFOUT(sd_out_sfilt) {
	int i, j;
	sd_out_sfilt_data *d = data;
	/* unused */ (void) nx; (void) nc;
	for (i=0; i<d->nfilt; i++) {
		d->x[i] = d->c[i] = 0.0;
		for (j=0; j<d->filtlen; j++) {
			d->x[i] += d->xfilts[i*d->filtlen+j] * x[j];
			d->c[i] += d->cfilts[i*d->filtlen+j] * c[j];
		}
	}
	return (*(d->out))(d->outd, t, d->nfilt, d->x, d->nfilt, d->c);
}

void sd_out_sfilt_free(sd_out_sfilt_data *d) {
	if (d->xfilts!=NULL) sd_free(d->xfilts);
	if (d->cfilts!=NULL) sd_free(d->cfilts);
	if (d->x!=NULL) sd_free(d->x);
	if (d->c!=NULL) sd_free(d->c);
	sd_free(d);
}

FILE *sd_out_file_get_fd(sd_out_file_data *d) { 
	RETNULL(d, 0);
	return d->fd;
}

int sd_out_mem_get_capacity(sd_out_mem_data *d) {
	RETNULL(d, 0);
       	return d->capacity; 
}

int sd_out_tee_get_nout(sd_out_tee_data *d) { RETNULL(d, 0);  return d->nout; }
int sd_out_tee_outs_is_null(sd_out_tee_data *d) { RETNULL(d, 1);  return d->outs == NULL; }
int sd_out_tee_outd_is_null(sd_out_tee_data *d) { RETNULL(d, 1);  return d->outd == NULL; }
sd_out sd_out_tee_get_out_i(sd_out_tee_data *d, int i) {
       	RETNULL(d, 0);  
	if (i>=0 && i<d->nout)
		return d->outs[i];
	sd_err("out of bounds getting tee out");
	return NULL;
}
void *sd_out_tee_get_outd_i(sd_out_tee_data *d, int i) {
       	RETNULL(d, 0);  
	if (i>=0 && i<d->nout)
		return d->outd[i];
	sd_err("out of bounds getting tee out user data.");
	return NULL;
}
int sd_out_tavg_get_len(sd_out_tavg_data *d) { RETNULL(d, 0);  return d->len; }
int sd_out_tavg_get_pos(sd_out_tavg_data *d) { RETNULL(d, 0);  return d->pos; }
double sd_out_tavg_get_t(sd_out_tavg_data *d) { RETNULL(d, 0);  return d->t; }
sd_out sd_out_tavg_get_out(sd_out_tavg_data *d) { RETNULL(d, 0);  return d->out; }
void *sd_out_tavg_get_outd(sd_out_tavg_data *d) { RETNULL(d, 0);  return d->outd; }
double *sd_out_tavg_get_x(sd_out_tavg_data *d) { RETNULL(d, 0);  return d->x; }
double *sd_out_tavg_get_c(sd_out_tavg_data *d) { RETNULL(d, 0);  return d->c; }
int sd_out_sfilt_get_nfilt(sd_out_sfilt_data *d) { RETNULL(d, 0);  return d->nfilt; }
int sd_out_sfilt_get_filtlen(sd_out_sfilt_data *d) { RETNULL(d, 0);  return d->filtlen; }
double *sd_out_sfilt_get_xfilts(sd_out_sfilt_data *d) { RETNULL(d, 0);  return d->xfilts; }
double *sd_out_sfilt_get_cfilts(sd_out_sfilt_data *d) { RETNULL(d, 0);  return d->cfilts; }
double *sd_out_sfilt_get_x(sd_out_sfilt_data *d) { RETNULL(d, 0);  return d->x; }
double *sd_out_sfilt_get_c(sd_out_sfilt_data *d) { RETNULL(d, 0);  return d->c; }
sd_out sd_out_sfilt_get_out(sd_out_sfilt_data *d) { RETNULL(d, 0);  return d->out; }
void *sd_out_sfilt_get_outd(sd_out_sfilt_data *d) { RETNULL(d, 0);  return d->outd; }
