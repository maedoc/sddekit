/* copyright 2016 Apache 2 sddekit authors */

#include "sddekit.h"

struct sd_sys_exc_dat {
    double a, tau, D, k;
};

sd_sys_exc_dat *sd_sys_exc_alloc() {
	return sd_malloc (sizeof(sd_sys_exc_dat));
}

void sd_sys_exc_free(sd_sys_exc_dat *d) {
	sd_free(d);
}

double sd_sys_exc_get_a(sd_sys_exc_dat *d) {
	return d->a;
}

void sd_sys_exc_set_a(sd_sys_exc_dat *d, double new_a) {
	d->a = new_a;
}

double sd_sys_exc_get_tau(sd_sys_exc_dat *d) {
	return d->tau;
}

void sd_sys_exc_set_tau(sd_sys_exc_dat *d, double new_tau) {
	d->tau = new_tau;
}

double sd_sys_exc_get_D(sd_sys_exc_dat *d) {
	return d->D;
}

void sd_sys_exc_set_D(sd_sys_exc_dat *d, double new_D) {
	d->D = new_D;
}

double sd_sys_exc_get_k(sd_sys_exc_dat *d) {
	return d->k;
}

void sd_sys_exc_set_k(sd_sys_exc_dat *d, double new_k) {
	d->k = new_k;
}

SK_DEFSYS(sd_sys_exc)
{
	sd_sys_exc_dat *d = data;
	/* avoid unused parameter warnings from compiler */
	(void) t; (void) nx; (void) nc; (void) hist;
	(void) F; (void) G; (void) Cf; (void) Cg;
	(void) i;
	/* evaluate system */
	f[0] = (x[0] - x[0]*x[0]*x[0]/3.0 + x[1]) * d->tau;
	f[1] = (d->a - x[0] + d->k*c[0]) / d->tau;
	g[0] = d->D;
	g[1] = d->D;
	c[0] = x[0];
	return 0;
}

#include <stdbool.h>
#include <stdint.h>
#define SK_API 

typedef struct { 
	uint32_t nx, nca, nce;
	bool have_jac;
} sd_sys_wc;

static
uint32_t 
get_nx(sd_sys_wc *sys)
{
	return sys->nx;
}

SK_API
sd_sys_wc *
sd_sys_wc_new() {
	sd_sys_wc *new = sd_malloc(sizeof(sd_sys_wc));
	new->nx = 2;
	new->nca = 0;
	new->nce = 2;
	return new;
}

SK_API
sd_sys *
sd_sys_wc_get_sys_vt(sd_sys_wc *sys)
{
	sd_sys *vt;
	(void) sys;
	vt = sd_malloc (sizeof(sd_sys));
	return vt;
}
