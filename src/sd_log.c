/* copyright 2016 Apache 2 sddekit authors */

#include "sddekit.h"

sd_log_msg_fp sd_log_msg = (sd_log_msg_fp) printf;
static int sd_log_quiet = 0;
static int sd_log_verbose = 1;

sd_log_msg_fp sd_log_get_msg() {
	return sd_log_msg;
}

void sd_log_set_msg(sd_log_msg_fp fp) {
	sd_log_msg = fp;
}

int sd_log_get_quiet() {
	return sd_log_quiet;
}

void sd_log_set_quiet(int flag) {
	sd_log_quiet = flag;
	if (flag)
		sd_log_verbose = 0;
}

int sd_log_get_verbose() {
	return sd_log_verbose;
}

void sd_log_set_verbose(int flag) {
	sd_log_verbose = flag;
	if (flag)
		sd_log_quiet = 0;
}
