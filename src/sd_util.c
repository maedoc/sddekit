/* copyright 2016 Apache 2 sddekit authors */

#include "sddekit.h"

static int compare_int(const void *a, const void *b)
{
	if ( *(int*) a <  *(int*) b ) return -1;
	if ( *(int*) a == *(int*) b ) return  0;
	else /* a > b */ return  1;
}

int sd_util_uniqi(
	const int n, 
	const int * restrict ints, 
	int * restrict nuniq, 
	int ** restrict uints)
{ 
	int i, j, *ints_copy;

	if (n==0) return 0;

	if (n==1) {
		*nuniq = 1;
		if ((*uints = sd_malloc (sizeof(int)))==NULL) {
			sd_err("failed to allocate memory for unique integers");
			return 1;
		}
		(*uints)[0] = ints[0];
		return 0;
	}

	/* sort copy of input vector */
	if ((ints_copy = (int*) sd_malloc(sizeof(int) * n))==NULL) {
		sd_err("failed to allocate memory for ints copy.");
		return 1;
	}
	memcpy(ints_copy, ints, n*sizeof(int));

	qsort(ints_copy, n, sizeof(int), compare_int);

	/* count uniq */
	*nuniq = 1;
	for (i=0; i<(n-1); i++)
		if (ints_copy[i] != ints_copy[i+1])
			(*nuniq)++;

	if ((*uints = (int*) sd_malloc (sizeof(int) * *nuniq))==NULL) {
		sd_err("failed to allocate memory for unique integers.");
		sd_free(ints_copy);
		return 1;
	}

	/* copy unique into output array */
	j = 0;
	(*uints)[j++] = ints_copy[0];
	for (i=0; i<(n-1); i++)
		if (ints_copy[i] != ints_copy[i+1])
			(*uints)[j++] = ints_copy[i+1];

	sd_free(ints_copy);
	
	return 0;
}

int sd_util_read_square_matrix(char *fname, int *n, double **w)
{
	int nn;
	FILE *fd;
	double _, *wi;
	/* open file */
	fd = fopen(fname, "r");
	if (fd==NULL) {
		sd_err("failed to open file");
		return 1;
	}
	/* count number of readable elements */
	nn = 0;
	{
		int count;
		while (1) {
			count = fscanf(fd, "%lg", &_);
			if (count < 1)
				break;
			nn++;
		}
	}
	rewind(fd);
	/* setup memory */
	*n = (int) sqrt(nn);
	wi = sd_malloc (sizeof(double)*nn);
	*w = wi;
	if (wi==NULL) {
		fclose(fd);
		return 1;
	}
	/* read data into memory this time */
	{
		int count;
		while (1) {
			count = fscanf(fd, "%lg", wi++);
			if (count < 1)
				break;
		}
	}
	fclose(fd);
	return 0;
}
