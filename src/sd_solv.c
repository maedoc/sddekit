/* copyright 2016 Apache 2 sddekit authors */

#include "sddekit.h"

struct sd_solv {
	int nx, nca, nce, cont;
	sd_sys *sys;
	sd_sch *sch;
	sd_out *out;
	void *sysd, *schd, *outd, *hfd;
	sd_hist_filler hf;
	sd_hist *hist; /* nd==nca, ci, cd */
	sd_rng rng; /* TODO mv to scheme? */
	double t, dt, *x, *c, *x0;
};

sd_solv *sd_solv_alloc() {
	sd_solv *new, zero = {0};
	new = sd_malloc (sizeof(sd_solv));
	*new = zero;
	return new;
}

int sd_solv_init(
	sd_solv * restrict s,
	sd_sys *sys, void * restrict sys_data,
	sd_sch *scheme, void * restrict scheme_data,
	sd_out *out, void * restrict out_data,
	sd_hist_filler hf, void * restrict hfill_data,
	int seed,
	int nx, double * restrict x0, int nce,
	int nca, int * restrict vi, double * restrict vd,
	double t0, double dt
	)
{
	char *errmsg;
#define FAILIF(cond, msg)\
       	if (cond) { \
		errmsg = msg;\
		goto uhoh;\
	}
	FAILIF(s==NULL, "cannot initialize NULL solver instance.")
	FAILIF(sys==NULL || scheme==NULL || hf==NULL,
	       "cannot initialize solver with NULL system, scheme or history function.")
	s->nx = nx;
	s->nce = nce;
	s->nca = nca;
	s->cont = 1;
	s->sys = sys;
#define WARNNULL(arg) if ((arg)==NULL)\
	sd_log_debug("solver init %s is NULL.", #arg)
	WARNNULL(s->sysd = sys_data);
	s->sch = scheme;
	WARNNULL(s->schd = scheme_data);
	WARNNULL(s->out = out);
	WARNNULL(s->outd = out_data);
	s->hf = hf;
	WARNNULL(s->hfd = hfill_data);
#undef WARNNULL
	FAILIF((s->x = sd_malloc (sizeof(double) * nx))==NULL, "failed to alloc solver x")
	FAILIF((s->x0 = sd_malloc (sizeof(double) * nx))==NULL, "failed to alloc solver x0")
	if (nca > 0 && vi!=NULL && vd!=NULL) 
        {
                size_t cn;
		FAILIF((s->hist = sd_hist_alloc())==NULL, "failed to allocate history.")
		FAILIF(sd_hist_init(s->hist, nca, vi, vd, t0, dt), "history init failed.")
		FAILIF(sd_hist_fill(s->hist, hf, hfill_data), "history fill failed.")
		/* s->c big enough to accomate aff or eff */
                cn = nca > nce ? nca : nce;
		FAILIF((s->c = sd_malloc(sizeof(double) * cn))==NULL,
			"alloc coupling array failed.")
	} else {
		s->nca = 0;
		s->c = NULL;
	}
	rk_seed(seed, &(s->rng));
	memcpy(s->x, x0, sizeof(double) * s->nx);
	memcpy(s->x0, x0, sizeof(double) * s->nx);
	s->t = t0;
	s->dt = dt;
	return 0;
uhoh:
	if (s->x!=NULL) sd_free(s->x);
	if (s->x0!=NULL) sd_free(s->x0);
	if (s->c!=NULL) sd_free(s->c);
	if (s->hist!=NULL) sd_hist_free(s->hist);
	sd_err(errmsg);
	return 1;
}

void sd_solv_free(sd_solv *s)
{
	sd_free(s->x);
	sd_free(s->x0);
	if (s->c != NULL) {
		sd_free(s->c);
		sd_hist_free(s->hist);
	}
	sd_free(s);
}

int sd_solv_cont(sd_solv *s)
{
	sd_hist *h;

	h = s->nca ? s->hist : NULL;
	s->cont = 1;
	do {
		if (s->sch(s->schd, h, &(s->rng), s->sys, s->sysd, s->t, s->dt,
		       s->nx, s->x, s->nce, s->c)) {
			sd_err("scheme failure.");
			return 1;
		}
		s->t += s->dt;
		s->cont = s->out(s->outd, s->t, s->nx, s->x, s->nce, s->c);
	} while (s->cont);

	return 0;
}

int sd_solv_get_nx(sd_solv *s) { return s->nx; }
int sd_solv_get_nce(sd_solv *s) { return s->nce; }
int sd_solv_get_nca(sd_solv *s) { return s->nca; }

 sd_hist *sd_solv_get_hist(sd_solv *s) { return  s->hist; }
 sd_rng  *sd_solv_get_rng (sd_solv *s) { return &s->rng ; }

double *sd_solv_get_c(sd_solv *s) { return s->c; }
double *sd_solv_get_x(sd_solv *s) { return s->x; }
double  sd_solv_get_t(sd_solv *s) { return s->t; }
