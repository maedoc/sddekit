/* copyright 2016 Apache 2 sddekit authors */

#include "sddekit.h"

struct sd_sch_id_data {
    int nx;
	double *f, *g, *z;
};

sd_sch_id_data *sd_sch_id_alloc() {
	sd_sch_id_data *new, zero = {0};
	new = sd_malloc (sizeof(sd_sch_id_data));
	*new = zero;
	return new;
}

int sd_sch_id_init(sd_sch_id_data *d, int nx)
{
	int err;
    d->nx = nx;
	err = 0;
	err |= (d->f=sd_malloc(sizeof(double)*nx))==NULL;
	err |= (d->g=sd_malloc(sizeof(double)*nx))==NULL;
	err |= (d->z=sd_malloc(sizeof(double)*nx))==NULL;
	if (err) {
		if (d->f==NULL) sd_free(d->f);
		if (d->g==NULL) sd_free(d->g);
		if (d->z==NULL) sd_free(d->z);
		sd_err("memory alloc durong sch id init failed.");
		return 1;
	}
	return 0;
}

int sd_sch_id_get_nx(sd_sch_id_data *d)
{
    return d->nx;
}

void sd_sch_id_free(sd_sch_id_data *d)
{
	if (d==NULL) {
		sd_err("returning early due to NULL instance pointer");
		return;
	}
	if (d->f!=NULL) sd_free(d->f);
	if (d->g!=NULL) sd_free(d->g);
	if (d->z!=NULL) sd_free(d->z);
	sd_free(d);
}

SK_DEFSCH(sd_sch_id) {
	int i, err;
	sd_sch_id_data *d;
	/* unused */ (void) dt;
	err = 0;
	d = data;
	sd_hist_get(hist, t, c);	      /*  Jf    Jg           Jc */
	err = (*sys)(sysd, hist, t, 0, nx, x, d->f, d->g, NULL, NULL, nc, c, NULL, NULL);
	if (err)
		return err;
	sd_util_fill_gauss(rng, nx, d->z);
	for (i=0; i<nx; i++)
		x[i] = d->f[i] + d->g[i] * d->z[i];
	sd_hist_set(hist, t, c);
	return 0;
}

struct sd_sch_em_data {
    int nx;
	double *f, *g, *z;
};

sd_sch_em_data *sd_sch_em_alloc() {
	sd_sch_em_data *new, zero = {0};
	new = sd_malloc (sizeof(sd_sch_em_data));
	*new = zero;
	return new;
}

int sd_sch_em_init(sd_sch_em_data *d, int nx)
{
	int err;
    d->nx = nx;
	err = 0;
	err |= (d->f=sd_malloc(sizeof(double)*nx))==NULL;
	err |= (d->g=sd_malloc(sizeof(double)*nx))==NULL;
	err |= (d->z=sd_malloc(sizeof(double)*nx))==NULL;
	if (err) {
		if (d->f!=NULL) sd_free(d->f);
		if (d->g!=NULL) sd_free(d->g);
		if (d->z!=NULL) sd_free(d->z);
		sd_err("memory alloc durong sch em init failed.");
		return 1;
	}
	return 0;
}

int sd_sch_em_get_nx(sd_sch_em_data *d)
{
    return d->nx;
}

void sd_sch_em_free(sd_sch_em_data *d)
{
	if (d==NULL) {
		sd_err("returning early due to NULL instance pointer");
		return;
	}
	if (d->f!=NULL) sd_free(d->f);
	if (d->g!=NULL) sd_free(d->g);
	if (d->z!=NULL) sd_free(d->z);
	sd_free(d);
}

SK_DEFSCH(sd_sch_em)
{
	int i, err;
	double sqrt_dt;
	sd_sch_em_data *d = data;
	err = 0;
	sd_hist_get(hist, t, c);	     /*  Jf    Jg           Jc */
	err = (*sys)(sysd, hist, t, 0, nx, x, d->f, d->g, NULL, NULL, nc, c, NULL, NULL);
	if (err)
		return 0;
	sd_util_fill_gauss(rng, nx, d->z);
	sqrt_dt = sqrt(dt);
	for (i=0; i<nx; i++)
		x[i] += dt * d->f[i] + sqrt_dt * d->g[i] * d->z[i];
	sd_hist_set(hist, t, c);
	return 0;
}

struct sd_sch_emcolor_data { 
	int first_call, nx;
	double *f, *g, *z, *eps, lam; 
};

sd_sch_emcolor_data *sd_sch_emcolor_alloc() {
	sd_sch_emcolor_data *new, zero = {0};
	new = sd_malloc (sizeof(sd_sch_emcolor_data));
	*new = zero;
	return new;
}

int sd_sch_emcolor_init(sd_sch_emcolor_data *d, int nx, double lam)
{
	int err;
    d->nx = nx;
	err = 0;
	err |= (d->f=sd_malloc(sizeof(double)*nx))==NULL;
	err |= (d->g=sd_malloc(sizeof(double)*nx))==NULL;
	err |= (d->z=sd_malloc(sizeof(double)*nx))==NULL;
	err |= (d->eps=sd_malloc(sizeof(double)*nx))==NULL;
	if (err) {
		if (d->f!=NULL) sd_free(d->f);
		if (d->g!=NULL) sd_free(d->g);
		if (d->z!=NULL) sd_free(d->z);
		if (d->eps!=NULL) sd_free(d->eps);
		sd_err("memory alloc durong sch em color init failed.");
		return 1;
	}
	d->first_call = 1;
	d->lam = lam;
	return 0;
}

int sd_sch_emcolor_get_nx(sd_sch_emcolor_data *d)
{
    return d->nx;
}

double sd_sch_emcolor_get_lam(sd_sch_emcolor_data *d)
{
    return d->lam;
}

void sd_sch_emcolor_set_lam(sd_sch_emcolor_data *d, double new_lam)
{
    d->lam = new_lam;
}

void sd_sch_emcolor_free(sd_sch_emcolor_data *d)
{
	if (d==NULL) {
		sd_err("returning early due to NULL instance pointer");
		return;
	}
	if (d->f!=NULL) sd_free(d->f);
	if (d->g!=NULL) sd_free(d->g);
	if (d->z!=NULL) sd_free(d->z);
	if (d->eps!=NULL) sd_free(d->eps);
	sd_free(d);
}

SK_DEFSCH(sd_sch_emcolor)
{
	int i, err;
	double E; /* not stored so can be chaned while running */
	sd_sch_emcolor_data *d = data;
	err = 0;
	if (d->first_call) {
		sd_util_fill_gauss(rng, nx, d->z);      /*  Jf    Jg           Jc */
		err = (*sys)(sysd, hist, t-dt, 0, nx, x, d->f, d->g, NULL, NULL, nc, c, NULL, NULL);
		if (err)
			return err;
		for (i=0; i<nx; i++)
			d->eps[i] = sqrt(d->g[i] * d->lam) * d->z[i];
		d->first_call = 0;
	}
	E = exp(-d->lam * dt);
	sd_util_fill_gauss(rng, nx, d->z);
	sd_hist_get(hist, t, c);	     /*  Jf    Jg           Jc */
	err = (*sys)(sysd, hist, t, 0, nx, x, d->f, d->g, NULL, NULL, nc, c, NULL, NULL);
	if (err)
		return err;
	for (i=0; i<nx; i++) {
		x[i] += dt * (d->f[i] + d->eps[i]);
		d->eps[i] *= E;
		d->eps[i] += sqrt(d->g[i] * d->lam * (1 - E*E)) * d->z[i];
	}
	sd_hist_set(hist, t, c);
	return 0;
}


struct sd_sch_heun_data {
    int nx;
	double *fl, *fr, *gl, *gr, *z, *xr;
};

sd_sch_heun_data *sd_sch_heun_alloc() {
	sd_sch_heun_data *new, zero = {0};
	new = sd_malloc (sizeof(sd_sch_heun_data));
	*new = zero;
	return new;
}

int sd_sch_heun_init(sd_sch_heun_data *d, int nx)
{
	int err;
    d->nx = nx;
	err = 0;
	err |= (d->fl=sd_malloc(sizeof(double)*nx))==NULL;
	err |= (d->fr=sd_malloc(sizeof(double)*nx))==NULL;
	err |= (d->gl=sd_malloc(sizeof(double)*nx))==NULL;
	err |= (d->gr=sd_malloc(sizeof(double)*nx))==NULL;
	err |= (d->z=sd_malloc(sizeof(double)*nx))==NULL;
	err |= (d->xr=sd_malloc(sizeof(double)*nx))==NULL;
	if (err) {
		if (d->fl!=NULL) sd_free(d->fl);;
		if (d->fr!=NULL) sd_free(d->fr);;
		if (d->gl!=NULL) sd_free(d->gl);;
		if (d->gr!=NULL) sd_free(d->gr);;
		if (d->z!=NULL) sd_free(d->z);;
		if (d->xr!=NULL) sd_free(d->xr);;
		sd_err("memory alloc durong sch em init failed.");
		return 1;
	}
	return 0;
}

int sd_sch_heun_get_nx(sd_sch_heun_data *d)
{
    return d->nx;
}

void sd_sch_heun_free(sd_sch_heun_data *d)
{
	if (d->fl!=NULL) sd_free(d->fl);;
	if (d->fr!=NULL) sd_free(d->fr);;
	if (d->gl!=NULL) sd_free(d->gl);;
	if (d->gr!=NULL) sd_free(d->gr);;
	if (d->z!=NULL) sd_free(d->z);;
	if (d->xr!=NULL) sd_free(d->xr);;
	sd_free(d);
}

SK_DEFSCH(sd_sch_heun)
{
	int i, err;
	double sqrt_dt;
	sd_sch_heun_data *d = data;
	/* predictor */
	sd_hist_get(hist, t, c);	     /*    Jf    Jg           Jc */
	err = (*sys)(sysd, hist, t, 0, nx, x, d->fl, d->gl, NULL, NULL, nc, c, NULL, NULL);
	if (err)
		return err;
	for (i=0; i<nx; i++)
		d->xr[i] = x[i] + dt * d->fl[i];
	sd_hist_set(hist, t, c);
	/* corrector */
	sd_hist_get(hist, t+dt, c);	            /*    Jf    Jg           Jc */
	err = (*sys)(sysd, hist, t+dt, 0, nx, d->xr, d->fr, d->gr, NULL, NULL, nc, c, NULL, NULL);
	if (err)
		return err;
	sd_util_fill_gauss(rng, nx, d->z);
	sqrt_dt = sqrt(dt);
	for (i=0; i<nx; i++)
		x[i] += 0.5 * (dt*(d->fl[i] + d->fr[i]) 
				+ sqrt_dt*(d->gl[i] + d->gr[i])*d->z[i]);
	/* TODO double check this has an effect */
	sd_hist_set(hist, t+dt, c);
	return 0;
}
