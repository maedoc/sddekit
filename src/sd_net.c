/* copyright 2016 Apache 2 sddekit authors */

#include "sddekit.h"

struct sd_net_data {
	int n, m, nnz, *M, *Ms, *Me, ns, ne, *Or, *Ic;
	double *w, *d, * restrict cn;
	sd_sys *models;
	void **models_data;
	/* flag for init1 use */
	int _init1;
};

sd_net_data *sd_net_alloc() {
	sd_net_data *new, zero = {0};
	new = sd_malloc (sizeof(sd_net_data));
	*new = zero;
	return new;
}

SK_DEFSYS(sd_net_sys)
{
	int l, j, mi, err;
	double *xi, *fi, *gi, *ci;
	sd_net_data *d = data;

	/* unused arguments */
	(void) nx; (void) F; (void) G; (void) Cf; (void) Cg; (void) nc;

	/* compute sparse coupling sum */
	for (l=0; l<d->ne; l++) 
		for (d->cn[l]=0.0, j=d->Or[l]; j<d->Or[l+1]; j++)
			/* c is len(ui), not ne! Ic[j] can't index it directly */
			d->cn[l] += c[sd_hist_get_vi2i(hist, d->Ic[j])] * d->w[j];

	/* TODO the main problem with this approach is that we don't know how many
	 * terms per system, so this completely if it isn't the same as nobs,
	 * or worse if the number varies from system to system.
	 */
	/* evaluate system(s) */
	if (F==NULL) {
		for  (l=0, xi=x, fi=f, gi=g, ci=d->cn; l<d->n; l++, 
		      xi+=d->Ms[mi], fi+=d->Ms[mi], gi+=d->Ms[mi], ci+=d->Me[mi]) 
		{
			mi = d->M[l];
			err = (*(d->models[mi]))(d->models_data[mi], hist, t, i,
				d->Ms[mi], xi, fi, gi, NULL, NULL,
				d->Me[mi], ci, NULL, NULL);
			if (err)
				return err;
		}
	} else {
		/* TODO evaluate & compute Jf/Jg/Jc 
		 * in net, we expect sys to evaluate only its own Jf/Jg, not whole.
		 * we are responsible for Jf/Jg ? or need Jc? Jca, Jce? 
		 *
		 * Jf/Jg include ca, i.e. [ df0/dx0 df0/dx1 .. df0/dca0 .. ] 
		 *
		 * may need Jc [ dce0/dx0 .. dce0/dca0 .. ]
		 *
		 * system defines Jf/Jg as block, we handle full network Jf/Jg
		 * which may be sparse if have large network.
		 * 
		 * assume sparse J by default: for small systems, penalty is small
		 * for large systems, need sparse; overall complexity is reduced.
		 */
	}
	/* compute coupling 
	 *
	 * solver deals with c following Ic,d, but here we wanted ce
	 * so we need to pack our cn into c.
	 */
	for (l=0; l<d->nnz; l++)
		c[l] = d->cn[d->Ic[l]];
	return 0;
}

struct sd_net_regmap_data {
	int i, *n;  /* n=1 for sum instead of averaging */
};

SK_DEFSYS(sd_net_regmap)
{
	int l;
	sd_net_regmap_data *d = data;
	/* unused arguments */
	(void) nx;(void) t; (void) F; (void) G; (void) Cf; (void) Cg; (void) hist; (void) i;
	f[0] = 0.0;
	g[0] = 0.0;
	x[0] = 0.0;
	for (l=0; l<nc; l++)
		x[0] += c[l];
	c[0] = x[0] / d->n[d->i];
	return 0;
}

int sd_net_init1(sd_net_data *net, int n, sd_sys sys, void *data, 
		int ns, int ne, int nnz, int *Or, int *Ic, double *w, double *d)
{
	int err, i, *M, *Ms, *Me;
	sd_sys *models;
	void **model_data;
	char *errmsg;
	err = (M = sd_malloc (sizeof(int) * n))==NULL;
	err |= (Ms = sd_malloc (sizeof(int)))==NULL;
	err |= (Me = sd_malloc (sizeof(int)))==NULL;
	err |= (models = sd_malloc (sizeof(sd_sys)))==NULL;
	err |= (model_data = sd_malloc (sizeof(void*)))==NULL;
	if (err) {
		errmsg = "failed to allocate net init1 storage.";
		goto fail;
	}
	Ms[0] = ns;
	Me[0] = ne;
	for(i=0; i<n; i++)
		M[i] = 0;
	models[0] = sys;
	model_data[0] = data;
	if (sd_net_initn(net, n, 1, M, Ms, Me, models, model_data, nnz, Or, Ic, w, d)) {
		errmsg = "net initn failed.";
		goto fail;
	}
	net->_init1 = 1;
	return 0;
fail:
	if (M!=NULL) sd_free(M);
	if (Ms!=NULL) sd_free(Ms);
	if (Me!=NULL) sd_free(Me);
	if (models!=NULL) sd_free(models);
	if (model_data!=NULL) sd_free(model_data);
	sd_err(errmsg);
	return 1;
}

void sd_net_free(sd_net_data *net)
{
	if (net->_init1) {
		sd_free(net->M);
		sd_free(net->Ms);
		sd_free(net->Me);
		sd_free(net->models);
		sd_free((void*) net->models_data);
		sd_free(net->cn);
	}
	sd_free(net);
}


int sd_net_initn(sd_net_data *net, int n, int m, 
		 int *M, int *Ms, int *Me, sd_sys *models, void **models_data,
		 int nnz, int *Or, int *Ic, double *w, double *d)
{
	int i, err;
	net->n = n;
	net->m = m;
	net->nnz = nnz;
	net->M = M;
	net->Ms = Ms;
	net->Me = Me;
	net->models = models;
	net->models_data = models_data;
	net->Or = Or;
	net->Ic = Ic; /* TODO ? same as Ie ? */
	net->w = w;
	net->d = d;
	/* intialize based on passed attributes: Ie, cne, cna */
	net->ns = 0;
	net->ne = 0;
	for (i=0; i<n; i++) {
		net->ns += net->Ms[net->M[i]];
		net->ne += net->Me[net->M[i]];
	}
	err = (net->cn = sd_malloc (sizeof(double) * net->ne))==NULL;
	if (err) {
		sd_err("failed to allocate memory for network.");
	}
	net->_init1 = 0;
	return err;
}

int sd_net_get_n(sd_net_data *net) {
	return net->n;
}

int sd_net_get_m(sd_net_data *net) {
	return net->m;
}

int sd_net_get_nnz(sd_net_data *net) {
	return net->nnz;
}

int *sd_net_get_or(sd_net_data *net) {
	return net->Or;
}

int sd_net_get_or_i(sd_net_data *net, int i) {
	return net->Or[i];
}

int *sd_net_get_ic(sd_net_data *net) {
	return net->Ic;
}

int sd_net_get_ic_i(sd_net_data *net, int i) {
	return net->Ic[i];
}

double *sd_net_get_w(sd_net_data *net) {
	return net->w;
}

double sd_net_get_w_i(sd_net_data *net, int i) {
	return net->w[i];
}

double *sd_net_get_d(sd_net_data *net) {
	return net->d;
}

double sd_net_get_d_i(sd_net_data *net, int i) {
	return net->d[i];
}

int sd_net_get_ns(sd_net_data *net) {
	return net->ns;
}

int sd_net_get_ne(sd_net_data *net) {
	return net->ne;
}

int sd_net_cn_is_null(sd_net_data *net) {
	return net->cn == NULL;
}

int sd_net_get_Ms_i(sd_net_data *net, int i) {
	return net->Ms[i];
}

int sd_net_get_Me_i(sd_net_data *net, int i) {
	return net->Me[i];
}

int sd_net_get_M_i(sd_net_data *net, int i) {
	return net->M[i];
}

sd_sys sd_net_get_models_i(sd_net_data *net, int i) {
	return net->models[i];
}

void *sd_net_get_models_data_i(sd_net_data *net, int i) {
	return net->models_data[i];
}

int sd_net_get__init1(sd_net_data *net) {
	return net->_init1;
}

