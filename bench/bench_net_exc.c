/* copyright 2016 Apache 2 sddekit authors */
#include <stdlib.h>
#include <stdio.h>

#include "sddekit.h"

typedef struct { sd_out out; void *outd; } bench_out_ignore_c_data;

SK_DEFOUT(bench_out_ignore_c) {
	bench_out_ignore_c_data *d = data;
	(void) nx; (void) nc; (void) c; /* unused */
	return (*(d->out))(d->outd, t, nx, x, 0, NULL);
}

typedef struct { double tf; } bench_out_stop_data;

SK_DEFOUT(bench_out_stop)
{
	bench_out_stop_data *d = data;
	(void) x; (void) nx; (void) nc; (void) c; /* unused */
	return t < d->tf;
}

int main() {
	int i, n, nnz, *Or, *Ic;
	double *w, *d, *sw, *sd, *x0;
	sd_out_tavg_data *tavgd;
	sd_out_file_data *filed;
	bench_out_stop_data stopd;
	sd_out_tee_data *teed;
	sd_sys_exc_dat *excd;
	sd_solv *solv;
	sd_sch_heun_data *heund;
	bench_out_ignore_c_data igncd;
	sd_net_data *netd;
	char *path;

	tavgd = sd_out_tavg_alloc();
	filed = sd_out_file_alloc();
	teed = sd_out_tee_alloc();
	excd = sd_sys_exc_alloc();
	solv = sd_solv_alloc();
	heund = sd_sch_heun_alloc();
	netd = sd_net_alloc();

	/* connectivity, assuming conduction velocity of 1.0 */
	sd_util_res_name("res/conn76/weights.txt", &path);
	sd_dat_read_square_matrix(path, &n, &w);
	sd_free(path);
	sd_util_res_name("res/conn76/tract_lengths.txt", &path);
	sd_dat_read_square_matrix(path, &n, &d);
	sd_free(path);
	sd_sparse_from_dense(n, n, w, d, 0.0, &nnz, &Or, &Ic, &sw, &sd);
	fprintf(stdout, "[bench_net_exc] nnz=%d\n", nnz);


	/* monitor to file */
	stopd.tf = 1e3;
	sd_out_file_from_fname(filed, "bench_net_exc.dat");
	sd_out_tavg_init(tavgd, 20, sd_out_file, filed);
	sd_out_tee_init(teed, 2);
	sd_out_tee_set_out(teed, 0, bench_out_stop, &stopd);
	sd_out_tee_set_out(teed, 1, sd_out_tavg, tavgd);
	igncd.out = sd_out_tee;
	igncd.outd = teed;

	/* setup model */
	sd_sys_exc_set_a(excd, 1.01);
	sd_sys_exc_set_tau(excd, 3.0);
	sd_sys_exc_set_D(excd, 0.01);
	sd_sys_exc_set_k(excd, 0.001);
	sd_net_init1(netd, n, sd_sys_exc, excd, 2, 1, nnz, Or, Ic, w, d);

	/* setup scheme & solver */
	x0 = sd_malloc (sizeof(double)*2*n);
	for (i=0; i<(2*n); i++)
		x0[i] = 0.0;
	sd_sch_heun_init(heund, 2*n);
	sd_solv_init(solv, sd_net_sys, netd, sd_sch_heun, heund,
			bench_out_ignore_c, &igncd, sd_hist_zero_filler, NULL,
			42, 2*n, x0, nnz, Ic, sd, 0.0, 0.01);

	/* solve */
	sd_solv_cont(solv);

	/* clean up */
	sd_free(w);
	sd_free(x0);
	sd_free(d);
	sd_free(Or);
	sd_free(Ic);
	sd_free(sw);
	sd_free(sd);
	sd_out_file_free(filed);
	sd_out_tavg_free(tavgd);
	sd_out_tee_free(teed);
	sd_solv_free(solv);

	return 0.0;
}
