/* copyright 2016 Apache 2 sddekit authors */

/* common headers */
#include "wps.h"

/* include all wrapper declarations */
#include "wps_rk_state.c"
#include "wps_sch.c"
#include "wps_hist.c"
#include "wps_solve.c"
