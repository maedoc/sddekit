# SDDEKit

[![Build Status](https://travis-ci.org/maedoc/sddekit.svg)](https://travis-ci.org/maedoc/sddekit)

SDDEKit is a toolbox for stochastic delay differential equations, with 
some support for networks and systems relevant to neuroscience.
